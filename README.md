# Desafio Magalu Ruan Souza

- `npm install`
- `npm run start:dev` - Rodar em modo desenvolvimento
- `start:prod` - Rodar em modo de produção

Em ambiente de desenvolvimento o mongo está rodando localmente

A aplicação está hospedada no heroku, e o banco está em um cluster no MongoDB Atlas
O desafio foi feito com o NestJs Framework, e a estrutura oferecida pelo framework foi aproveitada.

## Rotas

Para acessar a documentação da api é só acessar http://localhost:3000/api localmente ou com a url de produção https://luizalabs-desafio.herokuapp.com/api/
Todas as rotas estão documentadas no swagger.

## Autenticação

A autenticação é feita com JWT. Acesse a rota de login passando informações de email de um cliente criado. A rota de criação de cliente está desprotegida para permitir a criação em ambiente de desenvolvimento.
Todas as outras rotas estão protegidas, para autenticar passar o token da rota de login no Authorization no header.
